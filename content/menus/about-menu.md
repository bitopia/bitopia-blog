---
title: "About"
date: 2019-11-11T19:40:10+07:00
draft: false
menu: main
---

Welcome to **Bitopia**, now let me tell you what it is and how it came to be.

I, the author, am a software engineer - and like most software engineers I work on a lot of stuff in my spare time, just to scratch an itch or learn something new. I don't like repeating myself a lot so I have managed to build up a lot of infrastructure (both cloud based and self hosted) so I needed a domain name I could use with [Let's Encrypt]() and get SSL certificates for internal and external sites. A casual domain name search after a few beers tuned up `bitopia.net` as an available option and I grabbed it.

Of course that is not a very satisfying story so let my retro-actively change it. The 'Bit' part represents the quanta of computing, the 'Topia' is meant to imply utopia or perfection - so Bitopia represents the perfect computing environment for me as a programmer. And yes, there probably should be two t's in the middle, not one.

So what is the perfect environment (for me at least)? 

* An easy way to find out what I should talk to to get specific functionality I want to consume (file storage, data storage).
* A single source of events for everything happening, an easy way to trigger actions for those events.
* A way to deploy implementations of those actions regardless if they are complex (triggering a new ML training process) or simple (send a Tweet).
* A way to distribute all of this computational load across physical hardware in a fair way (and to let me know when I am starting to exceed my capacity).

There are a few other specific items (being able to trigger things through natural language interactions for example) but they all fit under the main categories above. I have spent at least 2 decades working on this (I still run code I originally wrote in the 90s, althought it has been rewritten many times) and worked through a lot of technologies from different periods trying to hit my goal.

There is a trade off between cost and benefit for everything (I am not Elon Musk) so I tend to migrate to the best available technology at the time that doesn't involve too much cost in time, money or effort. This blog is intended to describe that decision making process and how I implemented various parts of my system.
