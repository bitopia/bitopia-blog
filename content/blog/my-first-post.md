---
title: "My First Post"
date: 2019-11-11T13:04:27+07:00
draft: false
---

The first post for a new blog is always the hardest. What do I say here? Try to impress you with my skills and experience? Provide some flowery prose about all the wonderful things that will soon start to appear here?

Can't do it, sorry. I really just set this up for myself, obviously I do want others to read it and I hope it is actually interesting but I just wanted somewhere to clear the clutter in my head by spitting it out to another medium. It's cathartic.

I really do intend to adhere to a regular posting schedule but as they say '*the road to hell is paved with good intentions*' so in all honesty you will probably see periods of frantic activity punctuated by long drawn out pauses when something else has consumed all my interest for a while.

Anyway, dear reader, I hope what you see here is interesting, potentially useful or at least triggers some level of emotion in you.